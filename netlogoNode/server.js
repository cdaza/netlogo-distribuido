const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017/test_database';
const express = require("express");
const app = express();

let documentosJson;

let buscarDocumentos = (db) => {
    var collection = db.collection('agentes');

    collection.find().toArray(function(err,docs){
        if (err) throw err;

        let agentes = [];

        for ( index in docs){
            let doc = docs[index];
            let id = doc['_id'];
            let fabricante = doc['fabricante'];
            let velocidad = doc['velocidad'];
            let ubicacion = doc['ubicacion'];
            agentes.push({"id":id, "fabricante": fabricante, "velocidad": velocidad, "ubicacion": ubicacion })
        }
        documentosJson = {agentes}
    })

}

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.get("/agentesData", function(req, res){
    getData()
    res.send(documentosJson);
});

app.listen("3000", function(){
    console.log('Servidor en: localhost:3000');
});

function getData(){
    MongoClient.connect(url, function(err, client){
        if (err) throw err;
            buscarDocumentos(client.db('test_database'))
        });
  }