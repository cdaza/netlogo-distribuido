Netlogo distribuido <br>

Requerimientos: <br>
1.  Instalar mongoDB:<br>
En mac se puede usar brew para instalar mongoDB: <br>
<code>brew update</code> <br>
<code>brew install mongodb</code><br>
Despues de instalar mongo se debe crear la carpeta db donde se almacenarán las bases de datos. 
se puede crear en el directorio raiz con el siguiente comando : <br> 
<code>sudo mkdir -p /data/db</code> <br>
Para iniciar el servicio de mongo se usa el siguiente codigo: <br>
<code>mongod</code><br>

2.  Instalar node y npm con brew en Mac: <br>
<code>brew install node</code> <br>
Se puede validar la instalación con el siguiente comando: <br>
<code>node -v</code> <br>
3.  Instalar python con brew en Mac y luego instalar la libreria pymongo: <br>
<code>brew install python</code> <br>
<code>pip3 install pymongo</code> <br>
4.  Descargar la extensión de python para Netlogo: <br>
[Link de descarga](https://github.com/qiemem/PythonExtension/releases/tag/0.3.0)<br>
Descomprimir en la carpeta extensions dentro de la carpeta de netlogo<br>
5.  Para gestionar bases de datos mongo se puede usar el IDE Robo 3T: <br>
[Link de descarga](https://robomongo.org/download)

clonar el proyecto <br>
<code>git clone https://gitlab.com/cdaza/netlogo-distribuido.git</code>

abrir el archivo ejemploNetlogoDistribuido.nlogo en Netlogo. Ejecutar setup y luego go <br>
        
despues de clonar el proyecto. Con la consola sobre la carpeta netlogoNode se debe ejecutar la instalacion de las dependencias<br>
<code>npm install</code>
    
Para correr los ejemplos se debe ejecutar para iniciar el servidor<br>
<code>npm start</code>
    

   