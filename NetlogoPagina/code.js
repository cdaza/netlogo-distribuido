$(document).ready(function() {
    fetch('http://localhost:3000/agentesData')
    .then((resp) => resp.json())
    .then(function(data) {
        crearGraficaDistancia(data)
        crearGraficaCombustible(data)        
    })
    .catch(function(error) {
        console.log(JSON.stringify(error));
    }); 
    
    function crearGraficaDistancia(data) {

        let series = []; 
        $.each(data.agentes, function(i, item) {
            let distancias = [];
            $.each(item.ubicacion, function(i, ubicacion) {
                distancias.push(ubicacion.distancia)
            });
            series.push({ "name": "agente "+item.id, "data": distancias})
        });
        
        var title = {
            text: 'Distancia Agente al centro del mapa '   
         };
        var subtitle = {
            text: 'Fuente: Netlogo'
         };
        var xAxis = {
            title: {
               text: 'Movimientos'
            }
         };
        var yAxis = {
            title: {
               text: 'Distancia'
            },
            categories: ["agente 1","ag"],
            plotLines: [{
               value: 0,
               width: 1,
               color: '#808080'
            }]
         };   

         var tooltip = {
            valueSuffix: 'km'
         }
         var legend = {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
         };


        var json = {};
        json.title = title;
        json.subtitle = subtitle;
        json.xAxis = xAxis;
        json.yAxis = yAxis;
        json.tooltip = tooltip;
        json.legend = legend;
        json.series =series;
        $('#container').highcharts(json);

    }

    function crearGraficaCombustible(data) {

        let series = []; 
        let combustible = []
        $.each(data.agentes, function(i, item) {
            combustible.push(item.ubicacion.length)  
        });
        series.push({  "data": combustible })

        var chart = {
            type: 'column'
         };
         var title = {
            text: 'Consumo Combustible'   
         };
         var subtitle = {
            text: 'Fuente: Netlogo'  
         };
         var yAxis = {
            min: 0,
            title: {
               text: 'Combustible'         
            }      
         };

         var legend =  {
            enabled: false
         }
         var tooltip = {
            headerFormat: '<span style = "font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style = "color:{series.color};padding:0">Combustible:  </td>' +
               '<td style = "padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
         };
         var plotOptions = {
            column: {
               pointPadding: 0.2,
               borderWidth: 0
            }
         };  
         var credits = {
            enabled: false
         };   
      
         var json = {};   
         json.chart = chart; 
         json.title = title;   
         json.subtitle = subtitle; 
         json.tooltip = tooltip;
         json.yAxis = yAxis;  
         json.series = series;
         json.plotOptions = plotOptions; 
         json.legend = legend; 
         json.credits = credits;
         $('#container2').highcharts(json);

    }
 });